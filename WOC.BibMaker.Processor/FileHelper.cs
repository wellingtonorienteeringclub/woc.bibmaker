﻿using System.IO;

namespace WOC.BibMaker.Processor {
  public class FileHelper {
    private readonly string _RootFolder;

    public FileHelper(string rootFolder) {
      _RootFolder = rootFolder;
    }

    public string GetFullPath(string path) {
      if (Path.IsPathRooted(path)) {
        return path;
      }

      return Path.GetFullPath(Path.Combine(_RootFolder, path));
    }
  }
}