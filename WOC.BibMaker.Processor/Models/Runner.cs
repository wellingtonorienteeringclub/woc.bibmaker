﻿using System;
using System.Collections.Generic;

namespace WOC.BibMaker.Processor {
  public class Runner {
    public string FirstName { get; set; }
    public string Lastname { get; set; }
    public List<string> SIs { get; set; }
    public List<DateTime?> Times { get; set; }
    public List<string> Grades { get; set; }
    public string Club { get; set; }
  }
}