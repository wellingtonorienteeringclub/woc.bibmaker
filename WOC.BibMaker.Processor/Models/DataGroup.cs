﻿using System.Collections.Generic;

namespace WOC.BibMaker.Processor {
  public class DataGroup {
    public List<DataItem> Items { get; set; }
  }
}