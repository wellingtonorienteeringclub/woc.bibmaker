﻿namespace WOC.BibMaker.Processor {
  public class DataItem {
    public string Name { get; set; }
    public string Value { get; set; }
  }
}