﻿using System;
using System.Collections.Generic;
using System.Linq;
using IOF.XML.V3;

namespace WOC.BibMaker.Processor {
  public class IndividualRunnerService : IDataItemProvider {
    private readonly Settings _Settings;
    private readonly FileHelper _FileHelper;

    public IndividualRunnerService(Settings settings, FileHelper fileHelper) {
      _Settings = settings;
      _FileHelper = fileHelper;
    }

    public List<Runner> GetRunners() {
      var result = new Dictionary<string, Runner>();
      var files = _Settings.StartLists;

      for (var i = 0; i < files.Count; i++) {
        var startList = LoadStartlist(files[i]);

        foreach (var c in startList.ClassStart) {
          foreach (var personStart in c.PersonStart) {
            string firstName = personStart?.Person?.Name?.Given;
            string lastname = personStart?.Person?.Name?.Family;
            string si = personStart?.Start[0]?.ControlCard[0]?.Value;
            string club = personStart?.Organisation?.Name;
            string key = $"{firstName} {lastname} {club}";
            string grade = c.Class.Name;

            if (_Settings.Grades == null || _Settings.Grades.Count == 0 || _Settings.Grades.Contains(grade)) {
              if (!result.ContainsKey(key)) {
                result.Add(key, new Runner {
                  FirstName = firstName,
                  Lastname = lastname,
                  SIs = Enumerable.Range(0, files.Count).Select(p => "").ToList(),
                  Times = Enumerable.Range(0, files.Count).Select(p => (DateTime?)null).ToList(),
                  Grades = Enumerable.Range(0, files.Count).Select(p => "").ToList(),
                  Club = club,
                });
              }

              result[key].SIs[i] = si;
              result[key].Times[i] = personStart.Start[0].StartTime;
              result[key].Grades[i] = grade;
            }
          }
        }
      }

      return result.Values.OrderBy(p => p.Lastname).ThenBy(p => p.FirstName).ToList();
    }

    public List<DataGroup> GetPdfDataItem(List<Runner> runners) {
      var returnValue = new List<DataGroup>();

      foreach (var runner in runners) {
        var dataGroup = new List<DataItem>();
        dataGroup.Add(new DataItem { Name = "lastname", Value = runner.Lastname });
        dataGroup.Add(new DataItem { Name = "firstname", Value = runner.FirstName });
        dataGroup.Add(new DataItem { Name = "fullname", Value = (runner.FirstName + " " + runner.Lastname).Trim() });
        dataGroup.Add(new DataItem { Name = "club", Value = runner.Club });

        for (int i = 0; i < runner.Times.Count; i++) {
          var value = runner.Times[i].HasValue ? runner.Times[i].Value.ToString("HH:mm") : "";
          dataGroup.Add(new DataItem { Name = "start" + (i + 1), Value = value });
          dataGroup.Add(new DataItem { Name = "si" + (i + 1), Value = runner.SIs[i] });
          dataGroup.Add(new DataItem { Name = "grade" + (i + 1), Value = runner.Grades[i] });
        }

        returnValue.Add(new DataGroup { Items = dataGroup });
      };

      return returnValue;
    }


    public StartList LoadStartlist(string sourcePath) {
      string path = _FileHelper.GetFullPath(sourcePath);
      return Serializer.DeserializeXmlFromFile<StartList>(path);
    }

    public List<DataGroup> GetPdfDataItem() {
      var runners = GetRunners();
      return GetPdfDataItem(runners);
    }
  }
}