﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using Image = iTextSharp.text.Image;

namespace WOC.BibMaker.Processor {
  public class PdfMaker {
    private readonly Settings _Settings;
    private readonly FileHelper _FileHelper;

    public PdfMaker(Settings settings, FileHelper fileHelper) {
      _Settings = settings;
      _FileHelper = fileHelper;
    }

    public byte[] Create(List<DataGroup> dataGroups) {
      if (_Settings.NumberToProcess != null) {
        dataGroups = dataGroups.Take(_Settings.NumberToProcess.Value).ToList();
      }

      return CreateEmptyPdf(dataGroups);
    }

    private Image GetImage(string path) {
      return Image.GetInstance(_FileHelper.GetFullPath(path));
    }

    private byte[] CreateEmptyPdf(List<DataGroup> dataGroups) {
      float factor = 72f / 300f;

      MemoryStream ms = new MemoryStream();

      Document pdfDocument = new Document();
      PdfWriter writer = PdfWriter.GetInstance(pdfDocument, ms);
      pdfDocument.Open();
      pdfDocument.SetMargins(0, 0, 0, 0);

      pdfDocument.Add(new Paragraph("Empty"));

      for (var i = 0; i < dataGroups.Count; i++) {
        System.Console.WriteLine($"{i}/{dataGroups.Count}");

        PdfContentByte canvas = writer.DirectContent;

        float center = pdfDocument.Right / 2;
        float yOffSet = pdfDocument.Top / 2;

        if (i % 2 == 0) {
          pdfDocument.NewPage();
          yOffSet = 0;
        }

        var backgroundBottom = pdfDocument.Top / 2 - yOffSet;


        var backgroudImagePath = _Settings.Background.Default;

        if (_Settings.Background.Variations != null) {
          bool matched = false;

          foreach (var variation in _Settings.Background.Variations) {
            var variationValue = dataGroups[i].Items.FirstOrDefault(p => p.Name == variation.ValueSelector)?.Value;

            if (!String.IsNullOrWhiteSpace(variationValue)) {
              if (variation.MatchStrategy == "matches") {
                if (variation.Keys.Any(k => k == variationValue)) {
                  matched = true;
                }
              } else if (variation.MatchStrategy == "contains") {
                if (variation.Keys.Any(k => variationValue.Contains(k))) {
                  matched = true;
                }
              } else if (variation.MatchStrategy == "startswith") {
                if (variation.Keys.Any(k => variationValue.StartsWith(k))) {
                  matched = true;
                }
              }
            }

            if (matched) {
              backgroudImagePath = variation.Path;
              break;
            }
          }
        }

        if (!String.IsNullOrWhiteSpace(backgroudImagePath)) {
          var image = GetImage(backgroudImagePath);
          image.ScaleAbsolute(pdfDocument.Right, 1754 * factor);
          image.SetAbsolutePosition(0, backgroundBottom);
          pdfDocument.Add(image);
        }

        foreach (var element in _Settings.Elements) {
          if (element.Type == "Text") {
            WriteText(element, pdfDocument, canvas, yOffSet, factor, center, dataGroups[i]);
          }
        }
      }

      pdfDocument.Close();

      ms.Position = 0;

      PdfReader r = new PdfReader(ms);
      using (MemoryStream fs = new MemoryStream()) {
        Document doc = new Document();
        PdfWriter w = PdfWriter.GetInstance(doc, fs);
        doc.Open();
        for (int page = 1; page < r.NumberOfPages; page++) {
          doc.NewPage();
          w.DirectContent.AddTemplate(w.GetImportedPage(r, page + 1), 0, 0);
        }
        doc.Close();

        return fs.ToArray();
      }
    }

    private void WriteText(SettingsElement element, Document pdfDocument, PdfContentByte canvas, float yOffSet, float factor, float center, DataGroup dataGroup) {
      TextStyle textStyle = null;

      if (!String.IsNullOrWhiteSpace(element.TextStyle)) {
        textStyle = _Settings.TextStyles.FirstOrDefault(p => p.Name == element.TextStyle);
      }

      var elementTextAlign = !String.IsNullOrWhiteSpace(textStyle?.TextAlign) ? textStyle.TextAlign : element.TextAlign;
      var elementTextTransform = !String.IsNullOrWhiteSpace(textStyle?.TextTransform) ? textStyle.TextTransform : element.TextTransform;
      var elementTextRotation = textStyle?.TextRotation != null ? textStyle.TextRotation : element.TextRotation;
      var elementTextMargin = textStyle?.TextMargin != null ? textStyle.TextMargin.Value : element.TextMargin;
      var elementStroke = textStyle != null && textStyle.Stroke.HasValue && textStyle.Stroke.Value ? textStyle.Stroke.Value : element.Stroke;
      var elementStrokeColor = !String.IsNullOrWhiteSpace(textStyle?.StrokeColor) ? textStyle.StrokeColor : element.StrokeColor;
      var elementTextColor = !String.IsNullOrWhiteSpace(textStyle?.TextColor) ? textStyle.TextColor : element.TextColor;
      var elementStrokeWidth = textStyle?.StrokeWidth != null ? textStyle.StrokeWidth.Value : element.StrokeWidth;
      var elementFont = !String.IsNullOrWhiteSpace(textStyle?.Font) ? textStyle.Font : element.Font;
      var elementFontSize = textStyle?.FontSize != null ? textStyle.FontSize.Value : element.FontSize;
      var elementTextPosition = element.TextPosition;
      var elementTop = element.Top;
      var elementTextPrefix = element.TextPrefix;

      float top = pdfDocument.Top - yOffSet - elementTop * factor;
      int align = elementTextAlign == "Center" ? Element.ALIGN_CENTER : Element.ALIGN_LEFT;
      float pos = elementTextPosition == -1 ? center : elementTextPosition * factor;
      string value = dataGroup.Items.FirstOrDefault(p => p.Name == element.ValueSelector)?.Value + "";

      if (elementTextTransform == "Uppercase") {
        value = value.ToUpper();
      }

      if (!String.IsNullOrWhiteSpace(elementTextPrefix)) {
        value = elementTextPrefix + value;
      }

      if (elementStroke) {
        canvas.SetColorStroke(GetColor(elementStrokeColor));
        canvas.SetColorFill(GetColor(elementTextColor));
        canvas.SetLineWidth(elementStrokeWidth);

        canvas.SetTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
      } else {
        canvas.SetTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL);
      }

      Font font = GetFont(elementFont, elementTextColor, elementFontSize, value, pdfDocument.Right - elementTextMargin);

      float rotation = 0;

      if (elementTextRotation.HasValue) {
        rotation = elementTextRotation.Value;
      }

      Phrase phrase = new Phrase(value, font);
      ColumnText.ShowTextAligned(canvas, align, phrase, pos, top, rotation);
    }

    private BaseColor GetColor(string hexColor) {
      Color c = Color.FromArgb(int.Parse(hexColor.Replace("#", ""), System.Globalization.NumberStyles.AllowHexSpecifier));
      return new BaseColor(c);
    }

    private Font GetFont(string font, string textColor, float fontSize, string value, float maxWidth) {
      string fontPath = _FileHelper.GetFullPath(font);
      BaseColor color = GetColor(textColor);
      var customfont = BaseFont.CreateFont(fontPath, BaseFont.CP1252, BaseFont.EMBEDDED);

      float startSize = fontSize;

      float width = new Font(customfont).GetCalculatedBaseFont(false).GetWidthPoint(value, startSize);

      while (width > maxWidth) {
        width = new Font(customfont).GetCalculatedBaseFont(false).GetWidthPoint(value, --startSize);
      }

      return new Font(customfont, startSize, 0, color);
    }
  }
}