﻿using System.Collections.Generic;
using WOC.BibMaker.Processor;

namespace WOC.BibMaker.Console {
  public class RelayTeamService : IDataItemProvider {
    private readonly Settings _Settings;

    public RelayTeamService(Settings settings) {
      _Settings = settings;
    }

    public List<DataGroup> GetPdfDataItem() {
      List<DataGroup> result = new List<DataGroup>();

      foreach (var numberSet in _Settings.RelayNumbers) {
        for (var i = numberSet.Start; i < numberSet.Start + numberSet.NumberOfTeams; i++) {
          for (var j = 1; j <= numberSet.Legs; j++) {
            var group = new DataGroup { Items = new List<DataItem>() };
            group.Items.Add(new DataItem { Name = "number", Value = $"{i}" });
            group.Items.Add(new DataItem { Name = "numberandleg", Value = $"{i}{numberSet.Seperator}{j}" });
            group.Items.Add(new DataItem { Name = "legandnumber", Value = $"{j}{numberSet.Seperator}{i}" });
            group.Items.Add(new DataItem { Name = "leg", Value = $"{j}" });
            group.Items.Add(new DataItem { Name = "grade", Value = numberSet.Grade });

            result.Add(group);
          }
        }
      }

      return result;
    }
  }
}