﻿using System.Collections.Generic;

namespace WOC.BibMaker.Processor {
  public interface IDataItemProvider {
    List<DataGroup> GetPdfDataItem();
  }
}