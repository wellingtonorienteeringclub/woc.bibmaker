﻿using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace WOC.BibMaker.Processor {
  public static class Serializer {
    public static T DeserializeXmlFromFile<T>(string sourcePath) where T : class {
      XmlSerializer serializer = new XmlSerializer(typeof(T));
      using (StreamReader stream = new StreamReader(sourcePath)) {
        return serializer.Deserialize(stream) as T;
      }
    }

    public static string SerializeJson<T>(T instance) where T : class {
      return JsonConvert.SerializeObject(instance, Formatting.Indented);
    }

    public static T DeserializeJsonFromFile<T>(string sourcePath) {
      return DeserializeJson<T>(File.ReadAllText(sourcePath));
    }

    public static T DeserializeJson<T>(string rawJson) {
      return JsonConvert.DeserializeObject<T>(rawJson);
    }
  }
}