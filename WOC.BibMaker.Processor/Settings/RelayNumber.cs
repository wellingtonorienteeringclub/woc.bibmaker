﻿namespace WOC.BibMaker.Processor {
  public class RelayNumber {
    public int Start { get; set; }
    public int NumberOfTeams { get; set; }
    public int Legs { get; set; }
    public string Grade { get; set; }
    public string Seperator { get; set; }
  }
}