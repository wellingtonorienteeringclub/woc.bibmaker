﻿using System.Collections.Generic;

namespace WOC.BibMaker.Processor {

  public class Settings {
    public List<SettingsElement> Elements { get; set; }
    public List<TextStyle> TextStyles { get; set; }
    public BackgroundSettings Background { get; set; }
    public List<string> StartLists { get; set; }
    public List<string> Grades { get; set; }
    public int? NumberToProcess { get; set; }
    public string Name { get; set; }
    public string OutputPdfName { get; set; }
    public string Type { get; set; }
    public List<RelayNumber> RelayNumbers { get; set; }
  }
}