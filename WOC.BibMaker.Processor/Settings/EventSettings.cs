﻿using System.Collections.Generic;

namespace WOC.BibMaker.Processor {
  public class EventSettings {
    public List<Settings> Settings { get; set; }
    public string RootFolder { get; set; }
  }
}