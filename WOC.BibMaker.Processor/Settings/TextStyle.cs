﻿namespace WOC.BibMaker.Processor {
  public class TextStyle {
    public string Name { get; set; }
    public string Font { get; set; }
    public int? FontSize { get; set; }
    public string TextAlign { get; set; }
    public string TextTransform { get; set; }
    public float? TextMargin { get; set; }
    public string TextColor { get; set; }
    public int? TextRotation { get; set; }
    public bool? Stroke { get; set; }
    public string StrokeColor { get; set; }
    public float? StrokeWidth { get; set; }
  }
}