﻿using System.Collections.Generic;

namespace WOC.BibMaker.Processor {
  public class BackgroundSettings {
    public string Default { get; set; }
    public List<BackgroundVariationSettings> Variations { get; set; }
  }
}