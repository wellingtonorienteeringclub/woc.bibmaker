﻿using System.Collections.Generic;

namespace WOC.BibMaker.Processor {
  public class BackgroundVariationSettings {
    public string Path { get; set; }
    public List<string> Keys { get; set; }
    public string MatchStrategy { get; set; }
    public string ValueSelector { get; set; }
  }
}