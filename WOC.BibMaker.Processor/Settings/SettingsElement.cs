﻿namespace WOC.BibMaker.Processor {
  public class SettingsElement {
    public string Type { get; set; }
    public float Top { get; set; }
    public string Font { get; set; }
    public float FontSize { get; set; }
    public string TextAlign { get; set; }
    public float TextPosition { get; set; }
    public float TextMargin { get; set; }
    public string TextTransform { get; set; }
    public string TextColor { get; set; }
    public string ValueSelector { get; set; }
    public string TextPrefix { get; set; }
    public bool Stroke { get; set; }
    public string StrokeColor { get; set; }
    public float StrokeWidth { get; set; }
    public int? TextRotation { get; set; }
    public string TextStyle { get; set; }
  }
}