﻿using System.Collections.Generic;
using System.IO;
using WOC.BibMaker.Processor;

namespace WOC.BibMaker.Console {
  class Program {
    static void Main(string[] args) {
      var settings = LoadSettings();
      var fh = new FileHelper(settings.RootFolder);

      foreach (var setting in settings.Settings) {
        System.Console.WriteLine($"Processing {setting.Name}.");

        var maker = new PdfMaker(setting, fh);
        IDataItemProvider dataItemProvider = GetService(setting, fh);

        if (dataItemProvider == null) {
          continue;
        }

        List<DataGroup> pdfDataItem = dataItemProvider.GetPdfDataItem();

        var bytes = maker.Create(pdfDataItem);
        File.WriteAllBytes(fh.GetFullPath(setting.OutputPdfName), bytes);
      }

      System.Console.WriteLine("Done!");
      System.Console.ReadLine();
    }

    private static IDataItemProvider GetService(Settings setting, FileHelper fh) {
      if (setting.Type == "individual") {
        return new IndividualRunnerService(setting, fh);
      } else if (setting.Type == "relay") {
        return new RelayTeamService(setting);
      }

      return null;
    }

    private static EventSettings LoadSettings() {
      return Serializer.DeserializeJsonFromFile<EventSettings>("settings.json");
    }
  }
}